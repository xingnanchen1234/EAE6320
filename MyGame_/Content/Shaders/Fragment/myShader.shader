/*
	This is the standard fragment shader

	A fragment shader is responsible for telling the GPU what color a specific fragment should be
*/
#include <Shaders/shaders.inc>
#include <Shaders/contentbuffer.inc>

#if defined( EAE6320_PLATFORM_D3D )

// Entry Point
//============

void main(

	// Input
	//======

	in const float4 i_fragmentPosition : SV_POSITION,

	// Output
	//=======

	// Whatever color value is output from the fragment shader
	// will determine the color of the corresponding pixel on the screen
	out float4 o_color : SV_TARGET

)
{
	// Output solid white
	o_color = float4(
		// RGB (color)
		(sin( g_elapsedSecondCount_simulationTime )+1)/2, (1 + cos( g_elapsedSecondCount_simulationTime ))/2, 1,
		// Alpha (opacity)
		1.0 );
}

#elif defined( EAE6320_PLATFORM_GL )

// Output
//=======

// Whatever color value is output from the fragment shader
// will determine the color of the corresponding pixel on the screen
out vec4 o_color;

// Entry Point
//============

void main()
{
	// Output solid white
	o_color = vec4(
		// RGB (color)
		(sin( g_elapsedSecondCount_simulationTime )+1)/2, (1 + cos( g_elapsedSecondCount_simulationTime ))/2, 1,
		// Alpha (opacity)
		1.0 );
}

#endif
