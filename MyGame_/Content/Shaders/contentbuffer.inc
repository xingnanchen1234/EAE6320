// if included, must be included after shader.inc

DeclareConstantBuffer( g_constantBuffer_frame, 0 )
{
	matf4x4 g_transform_worldToCamera;
	matf4x4 g_transform_cameraToProjected;

	float g_elapsedSecondCount_systemTime;
	float g_elapsedSecondCount_simulationTime;
	// For float4 alignment
	vecf2 g_padding;
};