/*
	This file can be #included by both the C++ compiler and the resource compiler,
	and allows C++ code to refer to embedded resources symbolically
*/

#define IDI_EAEALIEN            101
#define IDI_EAEGAMEPAD          102

#define IDI_VSDEFAULT_LARGE     103
#define IDI_VSDEFAULT_SMALL     104
